# Important

Ce projet est une implémentation d'un typeur de Lambda-Calcul dans le cadre de l'UE *TAS* du *Master 2 STL* à Sorbonne Université fait par **Benslimane Amine** et **Sadat Walid**.

## Installation

Pour pouvoir compiler le programme, soyez sûr d'avoir installé *Ocaml* et *Opam*.

## Execution

Pour tester le typeur, lancez le script shell *run.sh*

```sh
sh run.sh
```
ou
```sh
./run.sh
```