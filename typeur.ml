(* Types *)
type 'a liste = Cons of 'a * ('a liste) | Nil

type lterme = Var of string | Abs of string * lterme | App of lterme * lterme | N of int | Unit
            | Add of lterme * lterme | Sub of lterme * lterme | Liste of (lterme liste) | PointFixe of string * lterme
            | Head of lterme | Tail of lterme | Izte of lterme * lterme * lterme | Iete of lterme * lterme * lterme
            | Let of string * lterme * lterme | Address of string | Deref of string | Ref of lterme | Assign of string * lterme

type stype =
              Var of string | Arrow of stype * stype | N | ListType of stype | Unit 
            | ForAll of string * stype | Ref of stype | WeakVar of string * stype * stype * bool
            | WeakForAll of string * stype * bool

type envi = (string * stype) list
type tequa = (stype * stype) list
type unif_state = Reussi | Continue | Rembobine | Echec_unification of string
type typage_res = lterme * stype * unif_state
type unif_res = tequa * unif_state

module Map = Map.Make(String)

(* Exceptions *)
exception VarPasTrouve
exception EquaPasTrouve
exception ErreurGeneration
exception ErreurEvaluation
exception AddressNull

(* Printer *)
let rec print_lterme (terme : lterme) : string = 
    match terme with
          Var x -> x
        | Abs (x, f) -> "(fun "^ x ^" -> " ^ (print_lterme f) ^")"
        | App (m, n) -> "(" ^ (print_lterme m) ^" "^ (print_lterme n) ^ ")"
        | N n -> string_of_int n
        | Add (x, y) -> "(" ^ (print_lterme x) ^ " + " ^ (print_lterme y) ^ ")"
        | Sub (x, y) -> "(" ^ (print_lterme x) ^ " - " ^ (print_lterme y) ^ ")"
        | Liste l -> print_liste l
        | Head l -> "(hd " ^ print_lterme l ^ ")"
        | Tail l -> "(tl " ^ print_lterme l ^ ")"
        | Izte (c, e1, e2) -> "(if0 " ^ (print_lterme c) ^ " then " ^ (print_lterme e1) ^ " else " ^ (print_lterme e2) ^ ")"
        | Iete (c, e1, e2) -> "(ifempty " ^ (print_lterme c) ^ " then " ^ (print_lterme e1) ^ " else " ^ (print_lterme e2) ^ ")"
        | PointFixe (phi, f) -> "Y (" ^ phi ^ " -> " ^ (print_lterme f) ^ ")"
        | Let (x, e1, e2) -> "(let " ^ x ^ " = " ^ (print_lterme e1) ^ " in " ^ (print_lterme e2) ^ ")"
        | Unit -> "()"
        | Address a -> a
        | Deref e -> "!" ^ e
        | Ref e -> "ref " ^ print_lterme e
        | Assign(e1, e2) -> e1 ^ " := " ^ print_lterme e2

and print_liste (l : lterme liste) : string =
    match l with
          Nil -> "[]"
        | Cons(h,t) -> "[" ^ (print_lterme h) ^ (let rec aux l =
                                                match l with
                                                    Nil -> "]"
                                                    |Cons(h',t') -> "," ^ (print_lterme h') ^ aux t'
                                            in aux t)

and print_reduced (l : lterme) : string = 
    print_lterme (ltrcbv l)

and print_stype (t : stype) : string = 
    match t with
         Var x -> x
        | Arrow (t1, t2) -> "("^ (print_stype t1) ^" -> " ^ (print_stype t2) ^")"
        | N -> "N"
        | ListType l -> "[" ^ (print_stype l) ^ "]"
        | ForAll (x,t) -> "∀" ^ x ^ "." ^ (print_stype t)
        | Unit -> "unit"
        | Ref t -> "Ref " ^ (print_stype t)
        | WeakVar(v, _, r, u) -> if u then print_stype r else v
        | WeakForAll (v, r, u) -> if u then print_stype r else "w∀(" ^ v ^ "." ^ print_stype r ^ ")"

and print_tequa (equas : tequa) : string =
    match equas with
    [] -> ""
    |(t1, t2)::t -> (print_stype t1) ^ " = " ^  (print_stype t2) ^ "\n" ^ print_tequa t

and print_typage_res (tr: typage_res) : string = 
    match tr with
         (t, res, Reussi) -> (print_lterme t)^" : TYPABLE avec le type "^(print_stype res)
        |(t, _, Echec_unification err) -> (print_lterme t)^" : PAS TYPABLE "^err
        | _ -> raise ErreurGeneration

(* Constructeurs *)
  (* ltermes *)
and cvar (x : string) : lterme = Var x
and cabs (x : string) (f : lterme) : lterme = Abs (x, f)
and capp (m : lterme) (n : lterme) : lterme = App (m, n)
and cn (n : int) : lterme = N n
and cadd (x : lterme) (y : lterme) : lterme = Add (x, y)
and csub (x : lterme) (y : lterme) : lterme = Sub (x, y)
and cliste (l : lterme list) : lterme =
    let rec aux l =
        match l with
            [] -> Nil
            |h::t -> Cons (h, aux t)
    in
        Liste (aux l)
and chead (l : lterme) : lterme = Head l
and ctail (l : lterme) : lterme = Tail l
and cif0 (c : lterme) (e1 : lterme) (e2 : lterme) : lterme = Izte (c, e1, e2)
and cifempty (c : lterme) (e1 : lterme) (e2 : lterme) : lterme = Iete (c, e1, e2)
and cpointfixe (phi : string) (f : lterme) : lterme = PointFixe (phi, f)
and clet (x: string) (e1: lterme) (e2 : lterme) : lterme = Let (x, e1, e2)
and cunit () : (lterme) = Unit
and caddr (a : string) : (lterme) = Address a
and cref (e : lterme) : (lterme) = Ref e
and cderef (e : string) : (lterme) = Deref e
and cassign (e1 : string) (e2 : lterme) : (lterme) = Assign(e1, e2)

  (* stypes *)
and cvartype (x : string) : stype = Var x
and carrow (t1 : stype) (t2 : stype) : stype = Arrow (t1, t2)
and cntype = N
and clisttype (t : stype) = ListType t
and cforall (x : string) (t : stype) = ForAll (x,t)
and cunittype () : (stype) = Unit
and creftype (t : stype) : (stype) = Ref t
and cweakvar (v : string) : (stype) = WeakVar(v, Unit, Unit, false)
and cweakforall (v : string) (t : stype) : (stype) = let ta = "_" ^ v in WeakForAll(ta, substitue v (cweakvar ta) t, false)

(* Sémantique *)
and cptVar : int ref = ref 0

and fresh_var () : string =
    cptVar := !cptVar + 1; 
    "L"^(string_of_int !cptVar)

and barendregt_rec rmap (l:lterme) : lterme =
    match l with
        Var x -> (try Var (Map.find x rmap) with
                        Not_found -> Var x)
        | Abs (x, t) -> let nx = (fresh_var ())  in Abs (nx, barendregt_rec (Map.add x nx rmap) t)
        | App (m, n) -> App ((barendregt_rec rmap m), (barendregt_rec rmap n))
        | Add (x, y) -> Add ((barendregt_rec rmap x),(barendregt_rec rmap y))
        | Sub (x, y) -> Sub ((barendregt_rec rmap x),(barendregt_rec rmap y))
        | Liste l -> Liste (barendregt_liste rmap l)
        | Head l -> Head (barendregt_rec rmap l)
        | Tail l -> Tail (barendregt_rec rmap l)
        | Izte (c, e1, e2) -> Izte ((barendregt_rec rmap c), (barendregt_rec rmap e1), (barendregt_rec rmap e2))
        | Iete (c, e1, e2) -> Iete ((barendregt_rec rmap c), (barendregt_rec rmap e1), (barendregt_rec rmap e2))
        | Let (x, e1, e2) -> let nx = (fresh_var ())  in Let (nx, barendregt_rec rmap e1, barendregt_rec (Map.add x nx rmap) e2)
        | _ -> l

and barendregt_liste rmap (l:lterme liste) : lterme liste =
    match l with
          Nil -> Nil
        | Cons(h,t) -> Cons(barendregt_rec rmap h, barendregt_liste rmap t)

and barendregt (l : lterme) : lterme =
    barendregt_rec Map.empty l

and instantie (x : string) (a : lterme) (l : lterme) : lterme =
    match l with
          Var y when y = x -> a
        | Abs (y, t) -> if (y = x) then (
            match a with
                Var v -> Abs (v, instantie x a t)
                | _ -> App (a, (instantie x a t))
        ) else Abs (y, (instantie x a t))
        | App (m, n) -> App ((instantie x a m), (instantie x a n))
        | Add (m, n) -> Add ((instantie x a m), (instantie x a n))
        | Sub (m, n) -> Sub ((instantie x a m), (instantie x a n))
        | Liste l -> Liste (instantie_liste x a l)
        | Head l -> Head (instantie x a l)
        | Tail l -> Tail (instantie x a l)
        | Izte (c, e1, e2) -> Izte ((instantie x a c), (instantie x a e1), (instantie x a e2))
        | Iete (c, e1, e2) -> Iete ((instantie x a c), (instantie x a e1), (instantie x a e2))
        | PointFixe (phi, f) -> if (phi = x) then (
                                            match a with
                                               Var v -> PointFixe(v, (instantie x a f))
                                              |_ -> l
                                            ) else PointFixe(phi ,(instantie x a f))
        | Let (y, e1, e2) -> if (y = x) then (
                                match a with
                                       Var v -> Let(v, instantie x a e1, instantie x a e2)
                                      | _ -> l
                            ) else Let (y, (instantie x a e1), (instantie x a e2))
        | _ -> l

and instantie_liste (x : string) (a : lterme) (l : lterme liste) : lterme liste =
    match l with
          Nil -> Nil
        | Cons(h,t) -> Cons(instantie x a h, instantie_liste x a t)

and ltrcbv_etape (l : lterme) (etat : (string * lterme) list) =
    match l with
        App(Abs (m,n),l) -> (instantie m l n, etat)
      | App(m,n) -> (try let (m', etat') = ltrcbv_etape m etat in (App(m', n), etat')
                        with ErreurEvaluation ->
                          let (n', etat') = ltrcbv_etape n etat in (App(m, n'), etat'))
      | Add (N n1, N n2) -> (N(n1 + n2), etat)
      | Add (m, n) -> (try let (m', etat') = ltrcbv_etape m etat in (Add(m', n), etat') 
                        with ErreurEvaluation ->
                          let (n', etat') = ltrcbv_etape n etat in (Add(m,n'), etat'))
      | Sub(N n1, N n2) -> (N(n1 - n2), etat)
      | Sub(m, n) -> (try let (m', etat') = ltrcbv_etape m etat in (Sub(m', n), etat') 
                        with ErreurEvaluation ->
                          let (n', etat') = ltrcbv_etape n etat in (Sub(m,n'), etat'))
      | Liste l -> ltrcbv_etape_liste l etat
      | Head l -> (try let (l', etat') = ltrcbv_etape l etat in (Head l', etat')
                                    with ErreurEvaluation ->
                                      match l with
                                          Liste (Cons (h,_)) -> (h, etat)
                                        | _ ->  raise ErreurEvaluation)
      | Tail l -> (try let (l', etat') = ltrcbv_etape l etat in (Tail l', etat')
                                    with ErreurEvaluation ->
                                      match l with
                                          Liste (Cons (_, t)) -> (Liste t, etat)
                                        | _ ->  raise ErreurEvaluation)
      | Izte (c, e1, e2) ->( try let (c', etat') = ltrcbv_etape c etat in (Izte(c', e1, e2), etat')
                            with ErreurEvaluation ->
                              (match c with
                                  N 0 -> (e1, etat)
                                | N _ -> (e2, etat)
                                | _ -> raise ErreurEvaluation
                              ))
      | Iete (c, e1, e2) -> (try let (c', etat') = ltrcbv_etape c etat in (Iete(c', e1, e2), etat')
                            with ErreurEvaluation ->
                              (match c with
                                  Liste Nil -> (e1, etat)
                                | Liste _ -> (e2, etat)
                                | _ -> raise ErreurEvaluation
                              ))
      | PointFixe (phi, f) -> (instantie phi l (barendregt f), etat)
      | Let (x, e1, e2) -> (try let (e1', etat') = ltrcbv_etape e1 etat in (Let(x, e1', e2), etat')
                            with ErreurEvaluation -> (instantie x e1 e2 , etat))
      | Ref e -> (try let (e', etat') = ltrcbv_etape e etat in (Ref e', etat')
                  with ErreurEvaluation -> let fa = fresh_var () in (Address fa, (fa,e)::etat))
      | Deref e -> (let rec aux l etat =
                      match etat with
                          [] -> raise AddressNull
                        | (e, a)::_ when e = l -> a
                        | (_::q) -> aux l q
                    in (aux e etat, etat))
      | Assign (x, e) -> (try let (e', etat') = ltrcbv_etape e etat in (Assign (x, e'), etat')
                          with ErreurEvaluation ->
                            let rec aux x l etat =
                              (match etat with
                                  [] -> raise AddressNull
                                | (e, a)::q when x = e -> (e, l)::q
                                | h::q -> h::aux x l q)
                            in (Unit, aux x e etat))
      | _ -> raise ErreurEvaluation

and ltrcbv_etape_liste (l : lterme liste) (etat : (string * lterme) list) =
  match l with
      Nil -> raise ErreurEvaluation
    | Cons(h, q) -> try let (h', etat') = (ltrcbv_etape h etat) in (Liste (Cons (h',q)), etat')
                      with ErreurEvaluation ->
                        let (q', etat') = (ltrcbv_etape_liste q etat) in 
                          (match q' with
                            Liste l' -> (Liste (Cons (h, l')), etat')
                            | _ -> raise ErreurEvaluation)

and ltrcbv_rec (l : lterme) (etat : (string * lterme) list) : lterme =
  try let (l', etat') = ltrcbv_etape l etat in ltrcbv_rec l' etat'
    with ErreurEvaluation -> l

and ltrcbv (l : lterme) : lterme =
    let l' = ltrcbv_rec (barendregt l) [] in
        l'

(* Types *)
and stype_egal (t1 : stype) (t2 : stype) : bool =
    match (t1, t2) with
         (Var x, Var y) when x = y -> true
        | (Arrow (xg, xd), Arrow (yg, yd)) -> (stype_egal xg yg) && (stype_egal xd yd)
        | (N, N) -> true
        | (ListType t1, ListType t2) -> stype_egal t1 t2
        | (ForAll (x1,t1), ForAll (x2,t2)) when x1 = x2 -> (stype_egal t1 t2)
        | (Unit, Unit) -> true
        | (Ref t1, Ref t2) -> stype_egal t1 t2
        | (WeakVar(v1, a1, _, u1), WeakVar(v2, a2, _, u2)) when u1 == u2 && v1 == v2 && u1 -> stype_egal a1 a2
        | (WeakForAll (v1, r1, u1), WeakForAll (v2, r2, u2)) when u1 == u2 && v1 == v2  && u1-> stype_egal r1 r2
        | _ -> false

(* Génération d'équations *)
and gen_equas (env : envi) (l : lterme) (t : stype) : tequa =
        match l with
             Var x -> [t,List.assoc x env]
            |Abs (x, m) -> let ta = fresh_var() in let tr = fresh_var () in
                (t,Arrow(Var ta, Var tr))::(gen_equas ((x,(Var ta))::env) m (Var tr))
            |App (m, n) -> let ta = fresh_var() in
                           let eq1 : tequa = gen_equas env m (Arrow(Var ta, t)) in
                           let eq2 : tequa = gen_equas env n (Var ta) in
                eq1@eq2
            |N _ -> [(t, N)]
            |Add (a, b) -> let eqa = gen_equas env a N in
                                let eqb = gen_equas env b N in
                                    (t,N)::(eqa @ eqb)
            |Sub (a, b) -> let eqa = gen_equas env a N in
                                let eqb = gen_equas env b N in
                                    (t,N)::(eqa @ eqb)
            |Liste l ->(match l with
                              Nil -> []
                            | Cons (h,r) -> let t1 = fresh_var() in
                                (t, ListType (Var t1))::((gen_equas env h (Var t1))@(gen_equas env (Liste r) (ListType (Var t1))))
                        )
            |Head l -> let ta = fresh_var() in (t, ForAll(ta, Arrow(ListType (Var ta), Var ta)))::(gen_equas env l (ListType (Var ta)))
            |Tail l -> let ta = fresh_var() in (t, ForAll(ta, Arrow(ListType (Var ta), ListType (Var ta))))::(gen_equas env l (ListType (Var ta)))
            |Izte (c, e1, e2) -> let ta = fresh_var () in (t,Var ta)::(gen_equas env c N)@(gen_equas env e1 (Var ta))@(gen_equas env e2 (Var ta))
            |Iete (c, e1, e2) -> let ta1 = fresh_var () in let ta2 = fresh_var () in (t, Var ta1)::(gen_equas env c (ListType (Var ta2)))@(gen_equas env e1 (Var ta1))@(gen_equas env e2 (Var ta1))
            |PointFixe (phi, f) -> let ta = fresh_var() in let rec aux env str1 str2 =
                                                                        match env with
                                                                              [] -> []
                                                                            | (s, t)::q when s = str1 -> (str2, t)::q
                                                                            | h::q -> h::aux q str1 str2
                                                                    in (t, Var ta)::(gen_equas (aux env phi ta) f (Var ta))
            |Let(x, e1, e2) -> (match (typeur env e1) with
                                (_, t0, Reussi) -> let rec aux e x t =
                                                    match e with
                                                      [] -> []
                                                      |(v,_)::q when x = v -> (v,t)::q
                                                      |h::q -> h::aux q x t
                                              in gen_equas (aux ((x, Var x)::env) x (generalise env t0)) e2 t
                                |_-> raise ErreurGeneration
                                )
            |Unit -> [(t, Unit)]
            |Ref _ -> let ta = fresh_var () in [(Var ta, ForAll(ta, Arrow (Var ta, Ref (Var ta))))]
            |Address _ -> raise ErreurGeneration
            |Deref e -> let ta = fresh_var () in [(Var ta, ForAll(ta, Arrow(Ref (Var ta), Var ta)))]
            |Assign(_, _) -> let ta = fresh_var () in [(Var ta, ForAll(ta, Arrow(Ref(Var ta), Arrow(Var ta, Unit))))]

and generalise (env : envi) (t : stype) : stype =
  let rec aux (l: string list) (t :stype) : (stype) =
    match l with
      [] -> t
      | h::q -> ForAll(h, aux q t)
    in aux (var_libres t) t

and generaliseWeak (env : envi) (t : stype) : stype =
  let rec aux (l : string list) (t :stype) : stype = 
    match l with 
        [] -> t
      | h::q -> cweakforall h (aux q t)
  in aux (var_libres t) t

and var_libres (t: stype) : (string list) =
    match t with
          Var x -> [x]
        | Arrow (t1, t2) -> let rec fusion l1 l2 = 
                                          match l2 with
                                              [] -> l1
                                            | h::q when listContainElem h l1 -> fusion l1 q
                                            | h::q -> fusion (h::l1) q
                            in fusion (var_libres t1) (var_libres t2)
        | N -> []
        | ListType l -> var_libres l
        | ForAll (x,t) -> let rec supprime_var var l =
                                              match l with 
                                                  [] -> []
                                                | h::q when h = var -> supprime_var var q
                                                | h::q -> h::supprime_var var q
                          in supprime_var x (var_libres t)
        | Unit -> []
        | Ref t -> var_libres t
        | WeakVar(v, a, _, u) -> if u then var_libres a else [v]
        | WeakForAll(v, r, u) -> if u then var_libres r else let rec supprime_var var l =
                                                                  match l with 
                                                                      [] -> []
                                                                    | h::q when h = var -> supprime_var var q
                                                                    | h::q -> h::supprime_var var q
                                                            in supprime_var v (var_libres r)

and listContainElem (str : string) (l : string list) : bool =
  match l with
  | [] -> false
  | (h::q) when h = str -> true
  | (h::q) -> listContainElem str q

and barendregt_rec_type rmap (t:stype) : stype =
    match t with
          Var x -> (try Var (Map.find x rmap) with
                    Not_found -> Var x)
        | Arrow (t1, t2) -> Arrow (barendregt_rec_type rmap t1, barendregt_rec_type rmap t2)
        | N -> N
        | ListType l -> ListType (barendregt_rec_type rmap l)
        | ForAll (x, s) -> let nx = fresh_var () in ForAll (nx, barendregt_rec_type (Map.add x nx rmap) s)
        | Ref t -> Ref (barendregt_rec_type rmap t)
        | Unit -> t
        | WeakVar(v, a, r, u) -> (if u then WeakVar(v, barendregt_rec_type rmap a, r , u)
                                      else try WeakVar((Map.find v rmap), a, r, u) with  Not_found -> Var v)
        | WeakForAll (v, r, u) -> if u then WeakForAll(v, barendregt_rec_type rmap r, u)
                                      else let ta = "_" ^ fresh_var() in let tr = barendregt_rec_type (Map.add v ta rmap) r
                                        in WeakForAll(v, tr, u)

and barendregt_type (t:stype) : stype =
    barendregt_rec_type Map.empty t

(* Unification *)
and occur_check(v : string) (t : stype) : bool =
    match t with
          Var x when x = v -> true
        | Arrow (t1, t2) -> (occur_check v t1) || (occur_check v t2)
        | ListType l -> occur_check v l
        | ForAll (_, t1) -> occur_check v t1
        | Ref t -> occur_check v t
        | WeakVar(e, _, r, u) -> if u then occur_check v r else v = e
        | WeakForAll (_, r, _) -> occur_check v r
        | _ -> false

and substitue (v : string) (ts : stype) (t : stype) : stype =
     match t with
          Var x when x = v -> ts
        | Var x -> Var x
        | Arrow (t1, t2) -> Arrow ((substitue v ts t1), (substitue v ts t2))
        | N -> N
        | ListType l -> ListType (substitue v ts l)
        | ForAll(x,l) when x = v -> ForAll (x, l)
        | ForAll(x,l) -> ForAll(x,substitue v ts l)
        | Ref t -> Ref (substitue v ts t)
        | Unit -> Unit
        | WeakVar(e, a, r, u) -> if u then WeakVar(e, a, substitue e ts r, u) else t
        | WeakForAll (e, r ,u) -> WeakForAll(v, substitue e ts r, u)

and substitue_partout (v : string) (ts : stype) (eqs : tequa) : tequa =
    List.map (fun (x,y) -> (substitue v ts x, substitue v ts y)) eqs

and trouve_but (e : tequa) (but : string) = 
  match e with
    [] -> raise VarPasTrouve
  | (Var v, t)::_ when v = but -> t
  | (t, Var v)::_ when v = but -> t 
  | c::e2 -> trouve_but e2 but

and unification (eqs: tequa) =
    let rec aux (eqs1: tequa) (eqs2 : tequa) =
        match eqs1 with
            [] -> (eqs2, Reussi)
            |(h::q) -> (
                match unification_etape h eqs2 with
                      (res, Continue) -> aux q res
                    | (res, Rembobine) -> aux res res
                    | (_, Echec_unification s) -> ([], Echec_unification s)
                    | _ -> ([], Echec_unification "Problème unification !")
            )
    in aux eqs eqs

and unification_etape (eqs1: stype * stype) (eqs2:tequa) =
    match eqs1 with        
        (a1, a2) when stype_egal a1 a2 -> (enlever_tequa  eqs1 eqs2, Rembobine) 
        | (ForAll(t1,t2), t3) -> ((barendregt_type t2, t3)::(enlever_tequa eqs1 eqs2), Rembobine)
        | (t3, ForAll(t1,t2)) -> ((t3, barendregt_type t2)::(enlever_tequa eqs1 eqs2), Rembobine)
        | (Var v1, t2) when v1 = "but" -> (eqs2, Continue)
        | (t1, Var v2) when v2 = "but" -> (eqs2, Continue)
        | (Var v1, Var v2) -> (substitue_partout v1 (Var v2) eqs2, Rembobine)
        | (Var v1, t2) ->  if occur_check v1 t2 then  ([], (Echec_unification ("occurence de "^ v1 ^" dans "^(print_stype t2)))) else (substitue_partout v1 t2 (enlever_tequa eqs1 eqs2), Rembobine)
        | (t1, Var v2) ->  if occur_check v2 t1 then  ([], (Echec_unification ("occurence de "^ v2 ^" dans "^(print_stype t1)))) else (substitue_partout v2 t1 (enlever_tequa eqs1 eqs2), Rembobine)
        | (Arrow (t1,t2), Arrow (t3, t4)) -> ((t1, t3)::(t2, t4)::enlever_tequa eqs1 eqs2, Continue)
        | (Arrow (_,_), t3) -> ([], (Echec_unification ("type fleche non-unifiable avec "^(print_stype t3))))
        | (t3, Arrow (_,_)) -> ([], (Echec_unification ("type fleche non-unifiable avec "^(print_stype t3))))    
        | (ListType t1, ListType t2) -> ((t1, t2)::(enlever_tequa eqs1 eqs2), Rembobine)
        | (ListType t1, t2) -> ([], (Echec_unification ("type liste non-unifiable avec "^(print_stype t2))))
        | (t2,ListType t1) -> ([], (Echec_unification ("type liste non-unifiable avec "^(print_stype t2))))
        | (N, N) -> (enlever_tequa eqs1 eqs2, Rembobine) 
        | (N, t3) -> ([], (Echec_unification ("type entier non-unifiable avec "^(print_stype t3))))
        | (t3, N) -> ([], (Echec_unification ("type entier non-unifiable avec "^(print_stype t3))))
        | (Ref t1, Ref t2) -> ((t1, t2)::enlever_tequa eqs1 eqs2, Rembobine)
        | (Ref t1, t2) -> ([], (Echec_unification ("type Ref non-unifiable avec "^(print_stype t2))))
        | (t1, Ref t2) -> ([], (Echec_unification ("type Ref non-unifiable avec "^(print_stype t1))))
        | (Unit, Unit) -> (enlever_tequa eqs1 eqs2, Rembobine) 
        | (WeakVar(v, _, _, _), t) when occur_check v t -> ([], Echec_unification ("variable faible " ^ v ^ " dans " ^ print_stype t))
        | (WeakVar(v, _, r, u), t) -> if u then ((r, t)::enlever_tequa eqs1 eqs2, Rembobine) else (enlever_tequa eqs1 eqs2, Rembobine)
        | (t, WeakVar(v, _, _, _)) when occur_check v t -> ([], Echec_unification ("variable faible " ^ v ^ " dans " ^ print_stype t))
        | (t, WeakVar(v, _, r, u)) -> if u then ((r, t)::enlever_tequa eqs1 eqs2, Rembobine) else (enlever_tequa eqs1 eqs2, Rembobine)
        | (WeakForAll (v, r, u), t) -> ((r,t)::enlever_tequa eqs1 eqs2, Rembobine)
        | (t,WeakForAll (v, r, u)) -> ((r,t)::enlever_tequa eqs1 eqs2, Rembobine)

and enlever_tequa (eq : stype * stype) (eqs : tequa) : (tequa) =
  match eqs with
  | [] -> raise EquaPasTrouve
  | (h::q) -> (match (eq, h) with
                    ((t1, t2), (t3, t4)) when (stype_egal t1 t3 && stype_egal t2 t4) -> q
                    | _ -> h::enlever_tequa eq q
                )
                
and typeur (env: envi) (t : lterme) : typage_res =
  try
    let eqs = gen_equas env t (Var "but") in
      match unification eqs with
          (res, Reussi) -> (try (t, trouve_but res "but", Reussi) with
                                    VarPasTrouve -> (t, Unit, Echec_unification("but pas trouvé ")))
        | (res, Echec_unification s) -> (t, N, Echec_unification((print_lterme t) ^ " pas typable : " ^ s)) 
        | (res, _) -> (t, N, Echec_unification "Problème unification !")
  with exc -> (t, N, Echec_unification "Problème typage !")

(* Tests *)  
let ex_id : lterme = Abs ("x", Var "x") 
let inf_ex_id : typage_res = typeur [] ex_id 
let ex_k : lterme = Abs ("x", Abs ("y", Var "x")) 
let inf_ex_k : typage_res = typeur [] ex_k
let ex_s : lterme = Abs ("x", Abs ("y", Abs ("z", App (App (Var "x", Var "z"), App (Var "y", Var "z")))))
let inf_ex_s : typage_res = typeur [] ex_s 
let ex_nat1 : lterme = App (Abs ("x", Add(Var "x", N 1)), N 3)
let inf_ex_nat1 : typage_res = typeur [] ex_nat1
let ex_nat2 : lterme = Abs ("x", Sub( Var "x", Var "x"))
let inf_ex_nat2 : typage_res = typeur [] ex_nat2
let ex_omega : lterme = App (Abs ("x", App (Var "x", Var "x")), Abs ("y", App (Var "y", Var "y")))
let inf_ex_omega : typage_res = typeur [] ex_omega
let ex_nat3 : lterme = App (ex_nat2, ex_id)
let inf_ex_nat3 : typage_res = typeur [] ex_nat3
let ex_liste1 : lterme = cliste [N 1; N 2; N 3]
let inf_ex_liste1 : typage_res = typeur [] ex_liste1
let ex_liste2 : lterme = cliste [N 1; N 2; ex_id]
let inf_ex_liste2 : typage_res = typeur [] ex_liste2
let ex_head1 : lterme = chead ex_liste1
let inf_ex_head1 : typage_res = typeur [] ex_head1
let ex_head2 : lterme = chead ex_liste2
let inf_ex_head2 : typage_res = typeur [] ex_head2
let ex_tail1 : lterme = ctail ex_liste1
let inf_ex_tail1 : typage_res = typeur [] ex_tail1
let ex_tail2 : lterme = ctail ex_liste2
let inf_ex_tail2 : typage_res = typeur [] ex_tail2
let ex_izte1 : lterme = cif0 (N 0) (ex_id) (Abs ("y", Var "y") )
let inf_ex_izte1 : typage_res = typeur [] ex_izte1
let ex_izte2 : lterme = cif0 (N 0) (ex_id) (ex_k)
let inf_ex_izte2 : typage_res = typeur [] ex_izte2
let ex_izte3 : lterme = cif0 (N 0) (N 1) (N 2)
let inf_ex_izte3 : typage_res = typeur [] ex_izte3
let ex_iete1 : lterme = cifempty ex_liste1 (ex_id) (Abs ("y", Var "y") )
let inf_ex_iete1 : typage_res = typeur [] ex_iete1
let ex_iete2 : lterme = cifempty ex_liste1 (ex_id) (ex_k)
let inf_ex_iete2 : typage_res = typeur [] ex_iete2
let ex_iete3 : lterme = cifempty ex_liste1 (N 1) (N 2)
let inf_ex_iete3 : typage_res = typeur [] ex_iete3
let ex_add1 : lterme = cadd (N 14) (N 11)
let inf_ex_add1 : typage_res = typeur [] ex_add1
let ex_add2 : lterme = cadd (N 14) (ex_id)
let inf_ex_add2 : typage_res = typeur [] ex_add2
let ex_sub1 : lterme = csub (N 14) (N 11)
let inf_ex_sub1 : typage_res = typeur [] ex_sub1
let ex_sub2 : lterme = csub (N 14) (ex_id)
let inf_ex_sub2 : typage_res = typeur [] ex_sub2
let ex_unit1 : lterme = cunit () 
let inf_ex_unit1 : typage_res = typeur [] ex_unit1
let ex_unit2 : lterme = Abs ("x", cunit ()) 
let inf_ex_unit2 : typage_res = typeur [] ex_unit2
let ex_let1 : lterme = clet "x" (N 14) (cadd (Var "x") (N 11))
let inf_ex_let1 : typage_res = typeur [] ex_let1
let ex_let2 : lterme = clet "x" (N 14) ex_id
let inf_ex_let2 : typage_res = typeur [] ex_let2
let ex_pointfix : lterme = cpointfixe "x" ex_id
let inf_ex_pointfix : typage_res = typeur [] ex_pointfix

let main () =
 print_endline "\n\nTYPAGE :";
 print_endline "======================";
 print_endline (print_typage_res inf_ex_id);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_k);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_s);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_omega);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_nat1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_nat2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_nat3);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_liste1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_liste2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_head1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_head2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_tail1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_tail2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_izte1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_izte2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_izte3);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_iete1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_iete2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_iete3);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_add1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_add2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_sub1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_sub2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_unit1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_unit2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_let1);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_let2);
 print_endline "======================";
 print_endline (print_typage_res inf_ex_pointfix);
 print_endline "======================";
 print_endline "\n\n\nSÉMANTIQUE :";
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_id);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_id);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_k);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_k);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_nat1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_nat1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_nat2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_nat2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_nat3);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_nat3);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_liste1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_liste1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_liste2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_liste2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_head1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_head1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_head2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_head2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_tail1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_tail1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_tail2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_tail2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_izte1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_izte1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_izte2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_izte2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_izte3);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_izte3);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_iete1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_iete1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_iete2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_iete2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_iete3);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_iete3);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_add1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_add1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_add2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_add2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_sub1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_sub1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_sub2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_sub2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_let1);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_let1);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_let2);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_let2);
 print_endline "======================";
 print_endline ("Avant ltrcbv : " ^ print_lterme ex_pointfix);
 print_endline ("Après ltrcbv : " ^ print_reduced ex_pointfix)

 let _ = main ()